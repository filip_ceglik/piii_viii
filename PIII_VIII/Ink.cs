namespace PIII_VIII
{
    public class Ink
    {
        public string color;
        public int quantity;

        public Ink(string color, int quantity)
        {
            this.color = color;
            this.quantity = quantity;
        }
    }
}