﻿using System;

namespace PIII_VIII
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            Printer localPrinter = new Printer();
            localPrinter.OutOfPaperEvent += OutOfPaperEventHandler;
            localPrinter.Print();
        }

        static void OutOfPaperEventHandler(object sender, EventArgs args)
        {
            Console.WriteLine("Out of paper!");
        }
    }
}