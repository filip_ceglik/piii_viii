using System.Collections.Generic;
using System.Drawing;

namespace PIII_VIII
{
    public class OutOfInkEventArgs
    {
        public Ink[] colors;

        public OutOfInkEventArgs(Ink[] colors)
        {
            this.colors = colors;
        }
    }
}