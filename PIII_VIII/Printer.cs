using System;
using System.Linq;

namespace PIII_VIII
{
    public class Printer
    {

        public Ink[] Inks;
        public void Print()
        {
            Console.WriteLine("Printing...");
            OutOfPaperEvent?.Invoke(this,EventArgs.Empty);
            OutOfInkEvent?.Invoke(this, new OutOfInkEventArgs(Inks.Where(x => x.quantity == 0).ToArray()));
        }

        public event EventHandler OutOfPaperEvent;
        public event EventHandler<OutOfInkEventArgs> OutOfInkEvent;

        private void OutOfPaperEventHandler(object sender, EventArgs args)
        {
            Console.WriteLine($"[PRINTER LOG {DateTime.Now.ToLongDateString()} | {DateTime.Now.ToLongTimeString()}]: Out of paper");
        }
        
        private void OutOfInkEventHandler(object sender, OutOfInkEventArgs args)
        {
            foreach (var item in args.colors)
            {
                Console.WriteLine($"[PRINTER LOG {DateTime.Now.ToLongDateString()} | {DateTime.Now.ToLongTimeString()}]: Out of {item.color} ink");
            }
        }

        public Printer()
        {
            this.Inks = new Ink[]{new Ink("yellow", 10), new Ink("magenta", 5), new Ink("cyan",0), new Ink("black", 3), };
            OutOfPaperEvent += OutOfPaperEventHandler;
            OutOfInkEvent += OutOfInkEventHandler;
        }
        
    }

    
}